﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Add two number and set result
        /// </summary>
        /// <param name="sender">reference to the element</param>
        /// <param name="e">event data</param>
        public void Addition(object sender,RoutedEventArgs e)
        {
            int exceptionOccurredTextBox = 1;
            try
            {
                int num1 = Int32.Parse(number1.Text);
                exceptionOccurredTextBox = 2;
                int num2 = Int32.Parse(number2.Text);
            Answer.Content = " The answer is " + (num1 + num2);
            AnswerBorder.Visibility = Visibility.Visible;
            }
            catch
            {
                MessageBox.Show("You haven't entered a valid number in this text box", "Error");
                if (exceptionOccurredTextBox == 1)
                    number1.Focus();
                else
                    number2.Focus();
            }
        }

        /// <summary>
        /// Add two number and set result
        /// </summary>
        /// <param name="sender">reference to the element</param>
        /// <param name="e">event data</param>
        public void Subtraction(object sender, RoutedEventArgs e)
        {
            int exceptionOccurredTextBox = 1;
            try
            {
                int num1 = Int32.Parse(number1.Text);
                exceptionOccurredTextBox = 2;
                int num2 = Int32.Parse(number2.Text);
                Answer.Content = " The answer is " + (num1 - num2);
                AnswerBorder.Visibility = Visibility.Visible;
            }
            catch
            {
                MessageBox.Show("You haven't entered a valid number in this text box", "Error");
                if (exceptionOccurredTextBox == 1)
                    number1.Focus();
                else
                    number2.Focus();
            }
        }

        /// <summary>
        /// Add two number and set result
        /// </summary>
        /// <param name="sender">reference to the element</param>
        /// <param name="e">event data</param>
        public void Multiplication(object sender, RoutedEventArgs e)
        {
            int exceptionOccurredTextBox = 1;
            try
            {
                int num1 = Int32.Parse(number1.Text);
                exceptionOccurredTextBox = 2;
                int num2 = Int32.Parse(number2.Text);
                Answer.Content = " The answer is " + (num1 * num2);
                AnswerBorder.Visibility = Visibility.Visible;
            }
            catch
            {
                MessageBox.Show("You haven't entered a valid number in this text box", "Error");
                if (exceptionOccurredTextBox == 1)
                    number1.Focus();
                else
                    number2.Focus();
            }
        }

        /// <summary>
        /// Add two number and set result
        /// </summary>
        /// <param name="sender">reference to the element</param>
        /// <param name="e">event data</param>
        public void Division(object sender, RoutedEventArgs e)
        {
            int exceptionOccurredTextBox = 1;
            try
            {
                int num1 = Int32.Parse(number1.Text);
                exceptionOccurredTextBox = 2;
                int num2 = Int32.Parse(number2.Text);
                double ans = num1 * 1.00 / num2;
                Answer.Content = " The answer is " + String.Format("{0:0.00}",ans);
                AnswerBorder.Visibility = Visibility.Visible;
            }
            catch
            {
                MessageBox.Show("You haven't entered a valid number in this text box", "Error");
                if (exceptionOccurredTextBox == 1)
                    number1.Focus();
                else
                    number2.Focus();
            }
        }
    }
}
